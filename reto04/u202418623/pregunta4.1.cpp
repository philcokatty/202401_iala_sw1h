#include <iostream>
#include <cstdlib>
#include <ctime>

using namespace std;

int* generarArregloAleatorio(int& tamano) {
    srand(time(nullptr));
    tamano = rand() % 401 + 100;
    int* arreglo = new int[tamano];
    for (int i = 0; i < tamano; ++i) {
        arreglo[i] = rand() % 10000 + 1;
    }
    return arreglo;
}

int main() {
    int tamano;
    int* arreglo = generarArregloAleatorio(tamano);
    cout << "Tama�o del arreglo: " << tamano << endl;
    cout << "Arreglo generado:" << endl;
    for (int i = 0; i < tamano; ++i) {
        cout << arreglo[i] << " ";
    }
    cout << endl;
    delete[] arreglo;
    system("pause");
    return 0;
}
