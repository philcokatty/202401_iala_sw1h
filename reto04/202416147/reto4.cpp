#include <iostream>
#include <cstdlib> // Para srand() y rand()
#include <ctime> // Para time()

// Funci�n para generar un arreglo de tama�o aleatorio con elementos aleatorios
int* generarArregloAleatorio(int& tamano) {
    // Generar un tama�o aleatorio entre 100 y 500
    tamano = rand() % 401 + 100;

    // Crear el arreglo din�micamente
    int* arreglo = new int[tamano];

    // Llenar el arreglo con elementos aleatorios entre 1 y 10000
    for (int i = 0; i < tamano; ++i) {
        arreglo[i] = rand() % 10000 + 1;
    }

    return arreglo;
}

int main() {
    // Inicializar la semilla para la generaci�n de n�meros aleatorios
    srand(time(NULL));

    // Declarar variable para almacenar el tama�o del arreglo generado
    int tamano;

    // Generar el arreglo aleatorio
    int* arreglo = generarArregloAleatorio(tamano);

    // Mostrar el tama�o del arreglo
    std::cout << "Tama�o del arreglo: " << tamano << std::endl;

    // Mostrar los elementos del arreglo
    std::cout << "Elementos del arreglo: ";
    for (int i = 0; i < tamano; ++i) {
        std::cout << arreglo[i] << " ";
    }
    std::cout << std::endl;

    // Liberar la memoria asignada al arreglo din�micamente
    delete[] arreglo;

    return 0;
}
