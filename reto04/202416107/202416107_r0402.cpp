##include <iostream>
#include <random>

using namespace std;

// Implemente una funci�n que permita crear una matrix din�mica de n x m, la inicialice con ceros y una funci�n que imprima la matrix en pantalla.

int randint(int min, int max, random_device& rd) {
    mt19937_64 gen(rd());
    return gen() % (max - min) + min;
}

int** randarray(int* rows, int* cols) {
    random_device rd;
    *rows = randint(0, 120, rd);
    *cols = randint(0, 30, rd);

    int** array;
    array = new int* [*rows];
    for (int i = 0; i < *rows; ++i) {
        array[i] = new int[*cols];
    }

    for (int i = 0; i < *rows; ++i) {
        for (int j = 0; j < *cols; ++j) {
            array[i][j] = 0;
        }
    }
    return array;
}

void showarray(int* rows, int* cols, int** array) {
    for (int i = 0; i < *rows; ++i) {
        for (int j = 0; j < *cols; ++j) {
            cout << array[i][j];
        }
        cout << "\n";
    }
}

/*CODIGO DE PRUEBA
int main()
{
    int* rows = new int;
    int* cols = new int;
    int** array;
    array = randarray(rows, cols);
    showarray(rows, cols, array);

    delete rows;
    delete cols;

    for (int i =0; i < rows; ++i){
    delete[] array[i];
    delete[]array;
    }

    system("pause>0");
    return EXIT_SUCCESS;
}
*/